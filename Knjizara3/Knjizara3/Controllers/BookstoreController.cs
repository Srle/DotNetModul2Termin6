﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knjizara3.Models;
using Knjizara3.ViewModels;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Knjizara3.Controllers
{
    public class BookstoreController : Controller
    {
        
        
        // GET: Bookstore
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddBookstore(string NameOfBookstore)
        {

                string query = "INSERT INTO AllBookstores (BookstoreName) VALUES (@namefbookstore);";
            /*query += " SELECT SCOPE_IDENTITY()";  */      // selektuj id novododatog zapisa nakon upisa u bazu

            string connectionString = ConfigurationManager.ConnectionStrings["AlephDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("@namefbookstore", NameOfBookstore); // stitimo od SQL Injection napada
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();          // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                           // zatvori konekciju
                }
            }
            return RedirectToAction("List");   // izlistaj ponovo spisak svih

        }


        public ActionResult List()
        {
            string query = "SELECT * FROM AllBookstores";  // upit nad bazom
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=AlephDB;Integrated Security=True";

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllBookstores"); // 'AllBookstores' je naziv tabele u dataset-u
                    dt = ds.Tables["AllBookstores"];    // formiraj DataTable na osnovu AllBookstores tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }
            }
            List<Bookstore> Bookstores = new List<Bookstore>();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int BookstoreId = int.Parse(dataRow["BookstoreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string BookstoreName = dataRow["BookstoreName"].ToString();

                Bookstores.Add(new Bookstore() { Id = BookstoreId, Name = BookstoreName });
            }

            return View(Bookstores);

        }


        public ActionResult Books( int id)
        {
            Project.st = id;
            int store = id;
            return RedirectToAction("List", "Book", id);
        }

    }





































}