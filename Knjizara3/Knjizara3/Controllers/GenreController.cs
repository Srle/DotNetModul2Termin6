﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knjizara3.Models;

namespace Knjizara3.Controllers
{
    public class GenreController : Controller
    {
        // GET: Genre
        public ActionResult Index()
        {
            return View(Project.AllGenres);
        }

        [HttpPost]
        public ActionResult AddGenre(string NameOfGenre)
        {
            int id = Project.AllGenres.Count + 1;
            Genre ge = new Genre(id, NameOfGenre, false);

            Project.AllGenres.Add(ge);

            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View(Project.AllGenres);
        }

        public ActionResult Delete(int id)
        {
            for (int i = 0; i < Project.AllGenres.Count; i++)
            {
                if (Project.AllGenres[i].Id == id)
                {
                    Project.AllGenres[i].Deleted = true;
                }
            }

            return RedirectToAction("Deleted");
        }

        public ActionResult Deleted()
        {
            return View(Project.AllGenres);
        }

        public ActionResult Edit(int id)
        {
            Genre nadjeni = new Genre();
            foreach (Genre ge in Project.AllGenres)
            {
                if (ge.Id == id)
                {
                    nadjeni = ge;
                }
            }
                return View(nadjeni);
        }

        [HttpPost]
        public ActionResult Edit(int IdOfGenre, string NewNameOfGenre)
        {

            for (int i=0; i< Project.AllGenres.Count; i++)
            {
                if (Project.AllGenres[i].Id == IdOfGenre)
                {
                    Project.AllGenres[i].Name = NewNameOfGenre;
                }
            }

            return RedirectToAction("List");
        }
    }
}