﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara3.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Genre Bookgenre { get; set; }
        public bool Deleted { get; set; }
        public List<Chapter> Chapters { get; set; }

        public Book()
        {
        }

        public Book(int id, string name, double price, bool deleted)
        {
            Bookgenre = new Genre();
            Chapters = new List<Chapter>();
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Deleted = deleted;
        }

        public Book(int id, string name, double price, Genre genre, bool deleted)
        {
            Chapters = new List<Chapter>();
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Bookgenre = genre;
            this.Deleted = deleted;
        }

        public Book(int id, string name, double price, Genre genre, bool deleted, List<Chapter> chapters)

        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Bookgenre = genre;
            this.Deleted = deleted;
            this.Chapters = chapters;
        }

    }
}