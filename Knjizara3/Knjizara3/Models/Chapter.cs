﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara3.Models
{
    public class Chapter
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Chapter()
        {
        }

        public Chapter(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}