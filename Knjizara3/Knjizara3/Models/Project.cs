﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara3.Models
{
    public class Project
    {
        public static List<Genre> AllGenres = new List<Genre>()
        {
            new Genre(1,"Science", false, Genrebk1),
            new Genre(2,"Comedy", false, Genrebk2),
            new Genre(3,"Horror", false, Genrebk3)
        };

        public static List<Book> Allbooks = new List<Book>()
        {
            new Book(1, "Prva Knjiga", 150.8, AllGenres[0], false, new List<Chapter>() { new Chapter(1,"Uvod 1"), new Chapter(2,"Razrada 1"), new Chapter(3,"Zakljucak 1")} ),
            new Book(2, "Druga Knjiga", 240.2, AllGenres[1], false, new List<Chapter>() { new Chapter(1,"Uvod 2"), new Chapter(2,"Razrada 2") } ),
            new Book(3, "Treca Knjiga", 170.9, AllGenres[2], false, new List<Chapter>() { new Chapter(2,"Razrada 3"), new Chapter(3,"Zakljucak 3")} ),
            new Book(4, "Cetvrta Knjiga", 152.1, AllGenres[1], false, new List<Chapter>() { new Chapter(1,"Uvod 4"), new Chapter(2,"Razrada 4"), new Chapter(3,"Zakljucak 4")} )
        };


        public static List<Book> Genrebk1 = new List<Book>() { Allbooks[0] };
        public static List<Book> Genrebk2 = new List<Book>() { Allbooks[1], Allbooks[3] };
        public static List<Book> Genrebk3 = new List<Book>() { Allbooks[2] };

        public static List<Bookstore> Allbookstores = new List<Bookstore>()
        {
            new Bookstore(1, "Knjizara 1", new List<Book>() { Allbooks[0], Allbooks[1], Allbooks[2], Allbooks[3] } ),
            new Bookstore(2, "Knjizara 2", new List<Book>() {Allbooks[1], Allbooks[3] } ),

        };

        public static int st = 0;

    }
}
