﻿--brisanje ranijih tabela ako postoje

drop table if exists [dbo].[BooksInBookstores];
drop table if exists [dbo].[AllBookstores];
drop table if exists [dbo].[AllBooks];
drop table if exists [dbo].[AllGenres];

--inicijalizacija tabela

create table [dbo].[AllGenres]
(
	[GenreId] int identity(1,1) primary key,
	[GenreName] nvarchar(20),
	[GenreDeleted] bit,
);

create table [dbo].[AllBooks]
(
	[BookId] int identity(1,1) primary key,
	[BookName] nvarchar(20),
	[BookPrice] float,
	[BookGenreId] int, 
	[BookDeleted] bit,
	foreign key (BookGenreId) references dbo.AllGenres(GenreId) on delete cascade
);

create table [dbo].[AllBookstores]
(
	[BookstoreId] int identity(1,1) primary key,
	[BookstoreName] nvarchar(20)
);

create table [dbo].[BooksInBookstores]
(
	[BookstoreId] int,
	[BookId] int,
	primary key([BookstoreId], [BookId]),
	foreign key (BookstoreId) references dbo.AllBookstores(BookstoreId) on delete cascade,
	foreign key (BookId) references dbo.AllBooks(BookId) on delete cascade
);

