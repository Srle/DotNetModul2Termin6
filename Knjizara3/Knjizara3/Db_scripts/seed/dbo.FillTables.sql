﻿--punjnje tabela
insert into dbo.AllGenres (GenreName, GenreDeleted) values ('Science', 0);
insert into dbo.AllGenres (GenreName, GenreDeleted) values ('Comedy', 0);
insert into dbo.AllGenres (GenreName, GenreDeleted) values ('Horror', 0);

insert into dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) values ('Prva Knjiga', 150.8, 1, 0);
insert into dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) values ('Druga Knjiga', 240.2, 2, 0);
insert into dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) values ('Treca Knjiga', 170.9, 3, 0);
insert into dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) values ('Cetvrta Knjiga', 152.1, 2, 0);

insert into dbo.AllBookstores (BookstoreName) values ('Knjizara 1');
insert into dbo.AllBookstores (BookstoreName) values ('Knjizara 2');

insert into dbo.BooksInBookstores (BookstoreId, BookId) values (1, 1);
insert into dbo.BooksInBookstores (BookstoreId, BookId) values (1, 2);
insert into dbo.BooksInBookstores (BookstoreId, BookId) values (1, 3);
insert into dbo.BooksInBookstores (BookstoreId, BookId) values (1, 4);
insert into dbo.BooksInBookstores (BookstoreId, BookId) values (2, 2);
insert into dbo.BooksInBookstores (BookstoreId, BookId) values (2, 4);     

-- select * from AllBooks;  select * from AllBookstores; select * from AllGenres;      delete from AllBookstores where BookstoreId = 3